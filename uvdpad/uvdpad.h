/*
 * Copyright (C) 2020  - All Rights Reserved
 * Author: Maxime Coquelin <maxime.coquelin@redhat.com>
 *
 * TBD: Licencing
 */

#ifndef __UVDPAD_H
#define __UVDPAD_H

#include "unix-socket.h"

enum vdpa_socket_mode {
	VDPA_IFACE_CLIENT,
	VDPA_IFACE_SERVER
};

struct vdpa_iface {
	TAILQ_ENTRY(vdpa_iface) next;
	struct rte_device *dev;
	char socket_path[PATH_MAX];
	enum vdpa_socket_mode socket_mode;
};

struct uvdpad {
	struct unix_socket socket;
	TAILQ_HEAD(, vdpa_iface) vdpa_ifaces;
};

#endif /* __UVDPAD_H */
