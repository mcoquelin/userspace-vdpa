/*
 * Copyright (C) 2020  - All Rights Reserved
 * Author: Maxime Coquelin <maxime.coquelin@redhat.com>
 *
 * TBD: Licencing
 */

#include <errno.h>
#include <rte_tailq.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "jsonrpc.h"
#include "unix-socket.h"

#define SELECT_TIMEOUT 500000

int unix_socket_create(struct unix_socket *usock)
{
	struct sockaddr_un sockaddr;
	int len, ret;

	/*
	 * Delete potential leftover socket file from previous run.
	 * Could happen in case of crash.
	 */
	ret = unlink(usock->path);
	if (ret < 0 && errno != ENOENT) {
		fprintf(stderr, "Failed to unlink socket: %s\n",
				strerror(errno));
		return -1;
	}

	TAILQ_INIT(&usock->clients);

	sockaddr.sun_family = AF_UNIX;
	strncpy(sockaddr.sun_path, usock->path, sizeof(sockaddr.sun_path));
	sockaddr.sun_path[sizeof(sockaddr.sun_path) - 1] = 0;
	len = sizeof(sockaddr.sun_family) + strlen(sockaddr.sun_path);

	usock->fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (usock->fd < 0) {
		fprintf(stderr, "socket() failed: %s\n",
				strerror(errno));
		return -1;
	}

	usock->maxfd = usock->fd + 1;

	ret = setsockopt(usock->fd, SOL_SOCKET, SO_REUSEADDR,
			&(int){1}, sizeof(int));
	if (ret < 0)
		fprintf(stderr, "Failed to set socket option: %s\n",
				strerror(errno));

	ret = bind(usock->fd, (struct sockaddr *)&sockaddr, len);
	if (ret < 0) {
		fprintf(stderr, "Failed to bind %s: %s\n",
				sockaddr.sun_path, strerror(errno));
		return -1;
	}

	ret = listen(usock->fd, 1);
	if (ret < 0) {
		fprintf(stderr, "Failed to listen %s: %s\n",
				sockaddr.sun_path, strerror(errno));
	}

	return 0;
}

static int unix_socket_accept(struct unix_socket *usock)
{
	int client_fd;
	struct unix_client *client;

	client_fd = accept(usock->fd, NULL, NULL);
	if (client_fd < 0) {
		fprintf(stderr, "Accept failed: %s\n", strerror(errno));
		return -1;
	}

	client = malloc(sizeof(struct unix_client));
	if (!client) {
		fprintf(stderr, "Failed to allocate client metadata\n");
		close(client_fd);
		return -1;
	}

	client->fd = client_fd;
	TAILQ_INSERT_TAIL(&usock->clients, client, next);
	if (client->fd >= usock->maxfd) {
		usock->maxfd = client->fd + 1;
	}

	fprintf(stdout, "Accept new client: %d\n", client_fd);

	return 0;
}

static void unix_client_close(struct unix_socket *usock,
							  struct unix_client *client)
{
	struct unix_client *tmp_client, *tmp;

	TAILQ_FOREACH_SAFE(tmp_client, &usock->clients, next, tmp) {
		if (tmp_client != client)
			continue;

		TAILQ_REMOVE(&usock->clients, tmp_client, next);
		close(client->fd);
		free(client);
		goto out;
	}

	fprintf(stderr, "Failed to close client (not found)\n");

out:
	usock->maxfd = usock->fd + 1;
	TAILQ_FOREACH(tmp_client, &usock->clients, next) {
		if (tmp_client->fd >= usock->maxfd)
			usock->maxfd = tmp_client->fd + 1;
	}
}

static void unix_client_req_handle(struct unix_socket *usock,
								   struct unix_client *client)
{
	char buf[8192]; //Todo: don't limit to a single recv, but loop
	char *reply;
	int ret;

	ret = recv(client->fd, buf, sizeof(buf) - 1, 0);
	if (ret == 0) {
		fprintf(stdout, "Client connection closed\n");
		unix_client_close(usock, client);
		return;
	} else if (ret < 0) {
		fprintf(stderr, "Recv error: %s\n", strerror(errno));
		unix_client_close(usock, client);
		return;
	} else if (ret >= (int)(sizeof(buf) - 1)) {
		fprintf(stderr, "Client request too long (%d Bytes)\n", ret);
		unix_client_close(usock, client);
		return;
	}

	buf[ret] = 0;
	fprintf(stdout, "Received request:\n%s\n", buf);

	reply = jsonrpc_handler(buf, ret, usock->jsonrpc_methods, NULL);

	if (reply) {
		fprintf(stdout, "Reply:\n%s\n", reply);
		ret = send(client->fd, reply, strlen(reply), MSG_WAITALL);
		if (ret == 0) {
			fprintf(stderr, "Client connection closed\n");
			unix_client_close(usock, client);
			return;
		} else if (ret < 0) {
			fprintf(stderr, "Send error: %s\n", strerror(errno));
			unix_client_close(usock, client);
			return;
		} else if (ret != (int)strlen(reply)) {
			fprintf(stderr, "Failed to send all reply (%d out of %d Bytes)\n",
					ret, (int)strlen(reply));
			unix_client_close(usock, client);
			return;
		}
	}

	return;
}

int unix_socket_process(struct unix_socket *usock)
{
	int ret;
	struct timeval timeout;
	fd_set fdset;
	struct unix_client *client, *tmp;

	FD_ZERO(&fdset);
	FD_SET(usock->fd, &fdset);
	TAILQ_FOREACH(client, &usock->clients, next) {
		FD_SET(client->fd, &fdset);
	}

	timeout.tv_sec = 0;
	timeout.tv_usec = SELECT_TIMEOUT;
	ret = select(usock->maxfd, &fdset, NULL, NULL, &timeout);
	if (ret < 0) {
		// ToDo: check catched signal
		fprintf(stderr, "Select failed: %s\n", strerror(errno));
		return -1;
	}

	if (ret == 0) {
		// Timeout
		return 0;
	}

	TAILQ_FOREACH_SAFE(client, &usock->clients, next, tmp) {
		if (FD_ISSET(client->fd, &fdset)) {
			unix_client_req_handle(usock, client);
		}
	}

	if (FD_ISSET(usock->fd, &fdset)) {
		// New client
		return unix_socket_accept(usock);
	}

	return 0;
}

int unix_socket_register_json(struct unix_socket *usock,
							  struct jsonrpc_method_entry_t *methods)
{
	usock->jsonrpc_methods = methods;

	return 0;
}
