/*
 * Copyright (C) 2020  - All Rights Reserved
 * Author: Maxime Coquelin <maxime.coquelin@redhat.com>
 *
 * TBD: Licencing
 */

#ifndef __UNIX_SOCKET_H
#define __UNIX_SOCKET_H

#include <sys/queue.h>

#include "jsonrpc.h"

struct unix_client {
	TAILQ_ENTRY(unix_client) next;
	int fd;
};

struct unix_socket {
	char *path;
	int fd;
	int maxfd;
	struct jsonrpc_method_entry_t *jsonrpc_methods;
	TAILQ_HEAD(, unix_client) clients;
};

int unix_socket_create(struct unix_socket *socket);
int unix_socket_process(struct unix_socket *socket);
int unix_socket_register_json(struct unix_socket *socket,
							  struct jsonrpc_method_entry_t *methods);

#endif /* __UNIX_SOCKET_H */
